Edit the file `/app/data/.env` to store the URL you wish to redirect to.

One line only, e.g. `https://sub.domain.tld`

Advise not to have blank lines after

Restart the app after editing .env
