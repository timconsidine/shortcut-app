"use strict";

var http = require("http");

const fs = require('fs');

try {
  const data = fs.readFileSync('/app/data/.env', 'utf8');
  console.log(data);
  var server = http.createServer(function (request, response) {
    // response.writeHead(200, {"Content-Type": "text/plain"});
    // response.end("Hello World\n");
    response.writeHead(302 , {
      'Location' : ''.concat(data.trim()) // This is your url which you want
    });
    response.end();
    });
  } catch (err) {
  console.error(err);
}

server.listen(3000);

console.log("Server running at port 3000");

