Simple app to host a link - based on cloudron tutorial nodejs app

Custom app : install it using the `cld.sh` build script
* ensure you have Docker installed
* ensure you have a Docker registry (Docker provide free hosted one or Cloudron has a self-hosted one)
* ensure you have Cloudron CLI installed (https://docs.cloudron.io/packaging/tutorial/#cloudron-cli)
* ensure you have logged in to your Cloudron instance with Cloudron CLI
* run the script : `./cld.sh <registry>/<app_repo>:<app_tag>`
* example : `./cld.sh docker.mycloudron.tld/shortcut-app:20220524`
* script will (1) do docker build, (2) do docker push to registry, (3) do cloudron install
* when prompted, enter Location of your app, e.g. `shortcut.mycloudron.tld` 

Edit the file `/app/data/.env` to store the URL you wish to redirect to.

One line only, e.g. `https://sub.domain.tld`

Advise not to have blank lines after

Restart the app after changing .env
